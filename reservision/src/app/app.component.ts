import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { SidenavComponent } from './sidenav/sidenav.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgApexchartsModule } from 'ng-apexcharts';
import { EvaluationChartComponent } from './sidenav/evaluation/evaluation-cart/evaluation-chart/evaluation-chart.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    CommonModule,
    RouterOutlet, 
    SidenavComponent,
    FormsModule,
    ReactiveFormsModule,
    NgApexchartsModule,
    EvaluationChartComponent
  ],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'] 
})

export class AppComponent {
  title = 'reservision';
}
