import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EvaluationComponent } from './evaluation/evaluation.component';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { VouchersComponent } from './vouchers/vouchers.component';

@Component({
  selector: 'app-sidenav',
  standalone: true,
  imports: [
    CommonModule,
    EvaluationComponent,
    VouchersComponent
  ],
  templateUrl: './sidenav.component.html',
  styleUrl: './sidenav.component.scss'
}) 

export class SidenavComponent {
  categorieNames: string[] = [
    'Quick-Statistik',
    'Gutscheine',
    'Kundendatenbank',
    'Auswertungen',
    'Einstellungen',
    'Designvorlagen',
    'Nutzerrechte',
    'Kassenschnittstelle'
  ];

  imageLogo: string = './assets/img/reservision-logo.png';

  selectedCategory: string | null = null;
  showCategories: boolean = false;
  isLargeScreen: boolean = false;
  imagePathSidenav: string = './assets/img/menu.png';

  constructor(private breakpointObserver: BreakpointObserver) {}

  ngOnInit(): void {
    this.breakpointObserver.observe([
      Breakpoints.Medium, 
      Breakpoints.Large, 
      Breakpoints.XLarge
    ]).subscribe(result => {
      if (result.matches) {
        this.isLargeScreen = true;
        this.showCategories = true; 
        if (!this.selectedCategory) {
          this.showContent('Auswertungen');
        }
      } else {
        this.isLargeScreen = false;
        this.showCategories = true; 
      }
    });
  }

  showContent(category: string): void {
    this.selectedCategory = category;
    this.showCategories = false;
  }

  closeCategory(): void {
    this.selectedCategory = null;
    this.showCategories = true; 
  }

  toggleCategories(): void {
    this.showCategories = !this.showCategories;
  }
}

