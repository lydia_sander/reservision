import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Coupons } from '../../../interfaces/coupons';

@Component({
  selector: 'app-vouchers',
  standalone: true,
  imports: [
    CommonModule
  ],
  templateUrl: './vouchers.component.html',
  styleUrl: './vouchers.component.scss'
})

export class VouchersComponent {
  redeemedVouchersheadline: string = "Eingelöste Gutescheine Heute"
  imgGift: string = './assets/img/gift.png'
  currentDate: Date = new Date();

  coupons: Coupons[] = [
    {
      name: "Max Mustermann",
      art: "Wertgutschein",
      voucherNumber: 12110,
      initialValue: 20,
      remainingValue: 0
    },
    {
      name: "Erika Musterfrau",
      art: "Wertgutschein",
      voucherNumber: 12111,
      initialValue: 40,
      remainingValue: 20
    },
    {
      name: "Hans Beispiel",
      art: "Wertgutschein",
      voucherNumber: 12112,
      initialValue: 30,
      remainingValue: 10
    }
  ];

  constructor() {}
}

