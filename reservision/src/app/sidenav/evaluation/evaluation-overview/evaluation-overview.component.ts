import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EvaluationTableComponent } from './evaluation-table/evaluation-table.component';

@Component({
  selector: 'app-evaluation-overview',
  standalone: true,
  imports: [
    CommonModule,
    EvaluationTableComponent
  ],
  templateUrl: './evaluation-overview.component.html',
  styleUrls: ['./evaluation-overview.component.scss']
})
export class EvaluationOverviewComponent {
    locationsTab: string[] = [
      'Gesamt',
      'Online',
      'Vor Ort'
    ];

    statusTab: string[] = [
      'Eingelöst',
      'Offen'
    ];

    selectedLocationTab: number | null = 0;
    selectedStatusTab: string | null = '';

    changeLocationTab(index: number) {
        this.selectedLocationTab = index;
        this.selectedStatusTab = null; 
    }

    changeStatusTab(status: string) {
        this.selectedStatusTab = status;
        this.selectedLocationTab = null; 
    }
}
