import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Order } from '../../../../../interfaces/order';

@Component({
  selector: 'app-evaluation-table',
  standalone: true,
  imports: [
    CommonModule
  ],
  templateUrl: './evaluation-table.component.html',
  styleUrls: ['./evaluation-table.component.scss']
})

export class EvaluationTableComponent {
  @Input() selectedLocationTab: number | null = 0;
  @Input() selectedStatusTab: string | null = '';

  dateTime: string = "Datum/Uhrzeit";
  customerName: string = "Name Käufer";
  employees: string = "Mitarbeiter/Mitarbeiter ID";
  art: string = "Art";
  number: string = "Anzahl";
  sum: string = "Betrag";

  orders: Order[] = [
    {
      date: '2024-04-20',
      time: '09:58',
      customerName: 'Max Mustermann',
      employees: 'Mitarbeiter name',
      employeesId: 'Mitarbeiter Id',
      coupon: 'Wertgutschein',
      couponId: 'A056D8',
      number: 2,
      sum: 20.00,
      ort: 'online',
      redeemed: 'yes'
    },
    {
      date: '2024-04-20',
      time: '09:23',
      customerName: 'Max Mustermann',
      employees: 'Mitarbeiter name',
      employeesId: 'Mitarbeiter Id',
      coupon: 'Wertgutschein',
      couponId: 'A056D8',
      number: 1,
      sum: 40.00,
      ort: 'online',
      redeemed: 'no'
    },
    {
      date: '2024-04-20',
      time: '09:12',
      customerName: 'Max Mustermann',
      employees: 'Mitarbeiter name',
      employeesId: 'Mitarbeiter Id',
      coupon: 'Wertgutschein',
      couponId: 'A056D8',
      number: 1,
      sum: 30.00,
      ort: 'vor Ort',
      redeemed: 'yes'
    },
  ];

  getFilteredOrders(): Order[] {
    let filteredOrders = this.orders;

    if (this.selectedLocationTab !== null) {
      switch (this.selectedLocationTab) {
        case 1: 
          filteredOrders = filteredOrders.filter(order => order.ort === 'online');
          break;
        case 2: 
          filteredOrders = filteredOrders.filter(order => order.ort === 'vor Ort');
          break;
        default: 
          break;
      }
    }

    if (this.selectedStatusTab !== null) {
      switch (this.selectedStatusTab) {
        case 'Eingelöst':
          return filteredOrders.filter(order => order.redeemed === 'yes');
        case 'Offen':
          return filteredOrders.filter(order => order.redeemed === 'no');
        default:
          return filteredOrders;
      }
    }

    return filteredOrders;
  }

  extraRows: null[] = Array.from({ length: 25 - this.orders.length });
  extraColumns: null[] = Array.from({ length: 6 });
}
