import { Component, Input, OnChanges, SimpleChanges } from "@angular/core";
import { NgApexchartsModule } from "ng-apexcharts";
import {
  ApexNonAxisChartSeries,
  ApexResponsive,
  ApexChart,
  ApexFill,
  ApexDataLabels,
  ApexTooltip,
  ApexStates,
  ApexStroke,
  ApexPlotOptions,
  ApexLegend
} from "ng-apexcharts";

export type ChartOptions = {
  series: ApexNonAxisChartSeries;
  chart: ApexChart;
  responsive: ApexResponsive[];
  fill: ApexFill;
  colors: string[];
  dataLabels: ApexDataLabels;
  tooltip: ApexTooltip;
  states: ApexStates;
  stroke: ApexStroke;
  plotOptions: ApexPlotOptions;
  legend: ApexLegend;  
};

@Component({
  selector: 'app-evaluation-chart',
  standalone: true,
  imports: [
    NgApexchartsModule
  ],
  templateUrl: './evaluation-chart.component.html',
  styleUrls: ['./evaluation-chart.component.scss']
})
export class EvaluationChartComponent implements OnChanges {
  @Input() series: ApexNonAxisChartSeries = [0, 0];
  public chartOptions: ChartOptions;

  constructor() {
    this.chartOptions = {
      series: this.series,
      chart: {
        width: 450,
        type: "pie"
      },
      colors: ["#737373", "#c4c2c2"], 
      fill: {
        colors: ["#737373", "#c4c2c2"]  
      },
      dataLabels: {
        enabled: true, 
        formatter: function (val: string | number) {
          const numVal = typeof val === 'number' ? Math.round(val) : Math.round(parseFloat(val.toString()));
          return numVal + "%";  
        },
        style: {
          fontSize: '30px',  
          fontFamily: 'Helvetica, Arial, sans-serif',
          fontWeight: 'bold',
          colors: ['#c4c2c2', '#737373']
        },
        dropShadow: {
          enabled: false  
        },
        background: {
          enabled: false  
        }
      },
      legend: {
        show: false  
      },
      tooltip: {
        enabled: false,  
        onDatasetHover: {
          highlightDataSeries: false,
        }
      },
      states: {
        hover: {
          filter: {
            type: 'none'  
          }
        },
        active: {
          filter: {
            type: 'none',  
            value: 0
          }
        }
      },
      stroke: {
        show: false 
      },
      plotOptions: {
        pie: {
          expandOnClick: false,  
          dataLabels: {
            offset: -40,
          }
        }
      },
      responsive: [
        {
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            dataLabels: {
              enabled: true, 
              formatter: function (val: string | number) {
                const numVal = typeof val === 'number' ? Math.round(val) : Math.round(parseFloat(val.toString()));
                return numVal + "%"; 
              },
              style: {
                fontSize: '16px',
                fontFamily: 'Helvetica, Arial, sans-serif',
                fontWeight: 'bold',
                colors: ['#FFF', '#000']
              },
              dropShadow: {
                enabled: false 
              }
            },
            tooltip: {
              enabled: false, 
              onDatasetHover: {
                highlightDataSeries: false,
              }
            },
            states: {
              hover: {
                filter: {
                  type: 'none' 
                }
              },
              active: {
                filter: {
                  type: 'none',  
                  value: 0
                }
              }
            },
            stroke: {
              show: false  
            },
            plotOptions: {
              pie: {
                expandOnClick: false,  
                dataLabels: {
                  offset: 0,  
                  minAngleToShowLabel: 10
                }
              }
            }
          }
        }
      ]
    };
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['series']) {
      this.chartOptions.series = this.series;
    }
  }
}
