import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EvaluationChartComponent } from './evaluation-chart/evaluation-chart.component';
import { Coupon } from './../../../../interfaces/coupon';
import { MonthData } from '../../../../interfaces/month-data';

@Component({
  selector: 'app-evaluation-cart',
  standalone: true,
  imports: [
    CommonModule,
    EvaluationChartComponent
  ],
  templateUrl: './evaluation-cart.component.html',
  styleUrls: ['./evaluation-cart.component.scss']
})

export class EvaluationCartComponent {
  evaluationCarts: string[] = [
    'Gesamt',
    'Tag',
    'Monat',
    'Quartal',
    'Halbjahr',
    'Jahr'
  ];

  dayCoupons: Coupon[] = [
    { redeemed: 20, open: 0, onlineRedeemed: 10, onlineOpen: 0, onSiteRedeemed: 10, onSiteOpen: 0 },
    { redeemed: 40, open: 20, onlineRedeemed: 20, onlineOpen: 10, onSiteRedeemed: 20, onSiteOpen: 10 },
    { redeemed: 30, open: 10, onlineRedeemed: 15, onlineOpen: 5, onSiteRedeemed: 15, onSiteOpen: 5 },
  ];

  currentMonthData: MonthData = { 
    redeemed: 250, open: 125, 
    onlineRedeemed: 100, onlineOpen: 50, 
    onSiteRedeemed: 150, onSiteOpen: 75 
  };

  monthsData: MonthData[] = [
    { redeemed: 100, open: 50, onlineRedeemed: 40, onlineOpen: 20, onSiteRedeemed: 60, onSiteOpen: 30 },
    { redeemed: 150, open: 75, onlineRedeemed: 60, onlineOpen: 30, onSiteRedeemed: 90, onSiteOpen: 45 },
    { redeemed: 200, open: 60, onlineRedeemed: 80, onlineOpen: 20, onSiteRedeemed: 120, onSiteOpen: 40 },
    { redeemed: 20, open: 200, onlineRedeemed: 10, onlineOpen: 100, onSiteRedeemed: 10, onSiteOpen: 100 },
    { redeemed: 90, open: 10, onlineRedeemed: 30, onlineOpen: 5, onSiteRedeemed: 60, onSiteOpen: 5 },
    { redeemed: 300, open: 20, onlineRedeemed: 150, onlineOpen: 10, onSiteRedeemed: 150, onSiteOpen: 10 },
    { redeemed: 80, open: 100, onlineRedeemed: 30, onlineOpen: 50, onSiteRedeemed: 50, onSiteOpen: 50 },
    { redeemed: 170, open: 100, onlineRedeemed: 70, onlineOpen: 40, onSiteRedeemed: 100, onSiteOpen: 60 },
    { redeemed: 110, open: 200, onlineRedeemed: 40, onlineOpen: 100, onSiteRedeemed: 70, onSiteOpen: 100 },
    { redeemed: 40, open: 80, onlineRedeemed: 20, onlineOpen: 40, onSiteRedeemed: 20, onSiteOpen: 40 },
    { redeemed: 130, open: 50, onlineRedeemed: 50, onlineOpen: 20, onSiteRedeemed: 80, onSiteOpen: 30 },
  ];

  redeemed: string = 'Eingelöst';
  open: string = 'Offen';
  total: string = 'Gesamt';
  online: string = 'Online';
  onSite: string = 'Vor Ort';

  selectedCart: string = '';
  series: number[] = [0, 0];

  constructor() { }

  selectCart(cart: string) {
    this.selectedCart = cart;
    this.updateChartValues();
  }

  ngOnInit() {
    this.selectedCart = 'Gesamt';
    this.updateChartValues();
  }

  generateNumbers(cart: string): { 
    redeemed: number, 
    open: number, 
    total: number, 
    online: { redeemed: number, open: number }, 
    onSite: { redeemed: number, open: number } 
  } 
  {
    let redeemedTotal = 0;
    let openTotal = 0;
    let onlineRedeemedTotal = 0;
    let onlineOpenTotal = 0;
    let onSiteRedeemedTotal = 0;
    let onSiteOpenTotal = 0;

    switch (cart) {
      case 'Gesamt':
        this.monthsData.forEach(month => {
          redeemedTotal += month.redeemed;
          openTotal += month.open;
          onlineRedeemedTotal += month.onlineRedeemed;
          onlineOpenTotal += month.onlineOpen;
          onSiteRedeemedTotal += month.onSiteRedeemed;
          onSiteOpenTotal += month.onSiteOpen;
        });
        break;
      case 'Tag':
        this.dayCoupons.forEach(coupon => {
          redeemedTotal += coupon.redeemed;
          openTotal += coupon.open;
          onlineRedeemedTotal += coupon.onlineRedeemed;
          onlineOpenTotal += coupon.onlineOpen;
          onSiteRedeemedTotal += coupon.onSiteRedeemed;
          onSiteOpenTotal += coupon.onSiteOpen;
        });
        break;
      case 'Monat':
        redeemedTotal = this.currentMonthData.redeemed;
        openTotal = this.currentMonthData.open;
        onlineRedeemedTotal = this.currentMonthData.onlineRedeemed;
        onlineOpenTotal = this.currentMonthData.onlineOpen;
        onSiteRedeemedTotal = this.currentMonthData.onSiteRedeemed;
        onSiteOpenTotal = this.currentMonthData.onSiteOpen;
        break;
      case 'Quartal':
        for (let i = 0; i < 3; i++) {
          redeemedTotal += this.monthsData[i].redeemed;
          openTotal += this.monthsData[i].open;
          onlineRedeemedTotal += this.monthsData[i].onlineRedeemed;
          onlineOpenTotal += this.monthsData[i].onlineOpen;
          onSiteRedeemedTotal += this.monthsData[i].onSiteRedeemed;
          onSiteOpenTotal += this.monthsData[i].onSiteOpen;
        }
        break;
      case 'Halbjahr':
        for (let i = 0; i < 6; i++) {
          redeemedTotal += this.monthsData[i].redeemed;
          openTotal += this.monthsData[i].open;
          onlineRedeemedTotal += this.monthsData[i].onlineRedeemed;
          onlineOpenTotal += this.monthsData[i].onlineOpen;
          onSiteRedeemedTotal += this.monthsData[i].onSiteRedeemed;
          onSiteOpenTotal += this.monthsData[i].onSiteOpen;
        }
        break;
      case 'Jahr':
        this.monthsData.forEach(month => {
          redeemedTotal += month.redeemed;
          openTotal += month.open;
          onlineRedeemedTotal += month.onlineRedeemed;
          onlineOpenTotal += month.onlineOpen;
          onSiteRedeemedTotal += month.onSiteRedeemed;
          onSiteOpenTotal += month.onSiteOpen;
        });
        break;
      default:
        break;
    }

    const total = redeemedTotal + openTotal;
    return { 
      redeemed: redeemedTotal, 
      open: openTotal, 
      total, 
      online: { redeemed: onlineRedeemedTotal, open: onlineOpenTotal }, 
      onSite: { redeemed: onSiteRedeemedTotal, open: onSiteOpenTotal } 
    };
  }

  updateChartValues() {
    const { online, onSite } = this.generateNumbers(this.selectedCart);
    const totalOnline = online.redeemed + online.open;
    const totalOnSite = onSite.redeemed + onSite.open;
    const total = totalOnline + totalOnSite;

    this.series = [
      (totalOnline / total) * 100, 
      (totalOnSite / total) * 100
    ];
  }
}
