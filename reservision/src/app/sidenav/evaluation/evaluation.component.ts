import { Component } from '@angular/core';
import { EvaluationCartComponent } from './evaluation-cart/evaluation-cart.component';
import { EvaluationOverviewComponent } from './evaluation-overview/evaluation-overview.component';

@Component({
  selector: 'app-evaluation',
  standalone: true,
  imports: [
    EvaluationCartComponent, 
    EvaluationOverviewComponent
  ],
  templateUrl: './evaluation.component.html',
  styleUrl: './evaluation.component.scss'
})

export class EvaluationComponent {
}
