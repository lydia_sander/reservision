export interface Order {
  date: string;
  time: string;
  customerName: string;
  employees: string;
  employeesId: string;
  coupon: string;
  couponId: string;
  number: number;
  sum: number;
  ort: string;
  redeemed: string;
}

