export interface MonthData {
    redeemed: number;
    open: number;
    onlineRedeemed: number;
    onlineOpen: number;
    onSiteRedeemed: number;
    onSiteOpen: number;
}
