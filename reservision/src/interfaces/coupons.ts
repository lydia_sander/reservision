export interface Coupons {
    name: string;
    art: string;
    voucherNumber: number;
    initialValue: number;
    remainingValue: number;
}
